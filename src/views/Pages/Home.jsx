import React from 'react';
import axios from 'axios';
import {API_URL} from "../../config/config"
import { Card, Col, Row, Image,Pagination } from 'antd';
import helpers from "../../lib/helpers";
import "../../style.css"

const { Meta } = Card;

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          count: 0,
        }
    }
    async componentWillMount(){
        let result = await axios.get(API_URL+"/products").then(function (response) {
            const {data} = response;
            return data;
          }).catch(error => {
            return [];
          });
        // let count = await axios.get(API_URL+"/products/count").then(function (response) {
        //     const {data} = response;
        //     return data;
        //   }).catch(error => {
        //     return [];
        //   });
          this.setState({data: result});
        //   debugger
    }

  render() {
    const {data,count} = this.state;
    return (
      <>
       <div className="container">
            <Row gutter={16}>
                {
                    data.map((item, idx) => {
                        return (
                            <Col span={8} key={idx}>
                              <Card
                                hoverable
                                style={{ width: 240 }}
                                cover={
                                  <Image
                                      width={240}
                                      height={240}
                                      src={`${API_URL}${item.image?.url}`}
                                  />
                                }
                              >
                                <Meta title={item.title} description={item.description} />
                                <span className="hp-money-title">Giá: <strong className="hp-money">{helpers.FormatNumber(item.price)}</strong></span>
                              </Card>
                            </Col>
                        );
                    })
                }
            </Row>
            {/* <Row gutter={16}>
              <Pagination
                  className="hp-mtb-10"
                  total={count}
                  onChange={this.onChangePaging}
                  showTotal={(total, range) => `${range[0]}-${range[1]} của ${total} mục`}
                  defaultPageSize={3}
                  defaultCurrent={1}
                />
            </Row> */}
        </div>
      </>
    );
  }
}

export default (Home);

