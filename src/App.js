import React, { Component } from 'react';
import { Route, Switch, Router } from 'react-router-dom';
import { history } from "./history";
import './App.css';
import 'antd/dist/antd.css';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;


// Pages
const Home = React.lazy(() => import('./views/Pages/Home'));


class App extends Component {

  render() {
    return (
      <Router history={history}>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route path="/" name="Home" render={props => <Home {...props}/>} />
            </Switch>
          </React.Suspense>
      </Router>
    );
  }
}

// export default App;
export default (App);