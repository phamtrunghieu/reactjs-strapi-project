import React from 'react';

// Home
const Home = React.lazy(() => import('./views/Pages/Home'));

const routes = [
  { path: '/', exact: true, name: 'Home', component: Home },
];

export default routes;
